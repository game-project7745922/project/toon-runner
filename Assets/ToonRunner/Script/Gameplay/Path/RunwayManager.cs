using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class RunwayManager : MonoBehaviour
{
    [SerializeField] private int _minObsShowed = 1;
    [SerializeField] private int _randomObsShowed = 0;
    [SerializeField] private ObstacleManager[] _obstacleManagers;

    List<ObstacleManager> _obstacle = new List<ObstacleManager>();

    private void Start()
    {
        InitObstacle();

    }

    public void InitObstacle()
    {
        Debug.Log($"{this.name}");

        foreach (var obstacle in _obstacleManagers)
        {
            obstacle.gameObject.SetActive(false);

            if (!_obstacle.Contains(obstacle))
                _obstacle.Add(obstacle);
        }

        _randomObsShowed = UnityEngine.Random.Range(_minObsShowed, _obstacle.Count);

        for (int i = 0; i < _randomObsShowed; i++)
        {
            int random = UnityEngine.Random.Range(0, _obstacle.Count - 1);

            _obstacle[random].gameObject.SetActive(true);

            ActivateRandomObs(random);

            _obstacle.Remove(_obstacle[random]);
        }

    }

    private void ActivateRandomObs(int random)
    {
        foreach (Transform obsItem in _obstacle[random].transform)
        {
            bool randomActive = (UnityEngine.Random.Range(0, 2) == 0) ? true : false;

            obsItem.gameObject.SetActive(randomActive);
        }

    }

}
