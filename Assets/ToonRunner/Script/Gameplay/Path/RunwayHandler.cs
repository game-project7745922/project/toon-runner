using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunwayHandler : MonoBehaviour
{
    [Tooltip("Z value from runaway to next runaway. Custom based on your runaway to next runaway z axis distance")]
    [SerializeField] private float _runwayPosZIncrement;
    [Tooltip("Number of runaway that will be spawned and reused in game")]
    [SerializeField] private int _pathInititated = 4;
    [SerializeField] private RunwayManager _runwayPrefab;

    private List<RunwayManager> _runways = new List<RunwayManager>();
    private float _lastRunawayPositionOnQueue;

    Queue<RunwayManager> _ranwaysQueue = new Queue<RunwayManager>();

    public float RunwayPosZIncrement => _runwayPosZIncrement;
    public float RastRunawayPositionOnQueue => _lastRunawayPositionOnQueue;
    public List<RunwayManager> Runways => _runways;

    private void Start()
    {
        for (int i = 0; i < _pathInititated; i++)
        {
            var runwaySpawned = Instantiate(_runwayPrefab, Vector3.forward * _lastRunawayPositionOnQueue, Quaternion.identity, this.transform);
            runwaySpawned.name = $"{runwaySpawned.name} {i}";

            _runways.Add(runwaySpawned);
            _ranwaysQueue.Enqueue(runwaySpawned);
            _lastRunawayPositionOnQueue += _runwayPosZIncrement;
        }

        Player.OnPassPath.AddListener(RepositionRunaway);
    }

    #region RepositionRunawaySummary
    /// <summary>
    /// <para> 
    /// Reposition runaway that has been passed by player to lastRunawayPositionOnQueue added it by runawayPosZIncrement.
    /// </para>
    /// <para> 
    /// On the process, dequeue the passed runaway and then enqueue it so it will be on the last queue of runaway
    /// </para> 
    /// </summary>
    #endregion
    private void RepositionRunaway()
    {
        var runaway = _ranwaysQueue.Dequeue();

        runaway.InitObstacle();

        _ranwaysQueue.Enqueue(runaway);

        runaway.transform.position = Vector3.forward * _lastRunawayPositionOnQueue;

        _lastRunawayPositionOnQueue += _runwayPosZIncrement;
    }

}
