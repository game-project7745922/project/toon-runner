using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    PlayerMovement playerMovement;

    //Ketika Player Melawati Trigger CheckPass dari runaway
    public static UnityEvent OnPassPath = new UnityEvent();
    public static UnityEvent OnCoinCollected = new UnityEvent();

    private void Awake()
    {
        playerMovement = GetComponent<PlayerMovement>();
    }

    private void Update()
    {
        playerMovement.DoMovement();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Coin")
            OnCoinCollected.Invoke();
            
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "CheckPassRunaway")
            OnPassPath.Invoke();

    }


}
