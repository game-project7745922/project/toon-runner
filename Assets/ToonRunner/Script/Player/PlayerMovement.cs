using DG.Tweening;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement value")]
    [SerializeField] private float _speed = 1.0f;
    [SerializeField] private float _jumpForce = 1.0f;

    [Header("Ground Configuration")]
    [Tooltip("For ground detection, detect ground to step on by layer.")]
    [SerializeField] private LayerMask _groundMask;
    [Tooltip("Is on ground check")]
    private bool isOnGround = false;

    [Header("Lane Configuration")]
    [Tooltip("Configure lane point. Lane point is player horizontal pivot location in X axis.")]
    [SerializeField] private float[] _lanePoint;
    [Tooltip("Configure player duration to move to lane point.")]
    [SerializeField] private float _changeLaneDuration = 1.0f;


    private int _lane = 1;
    Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }


    void Start()
    {
        float currentPosY = transform.position.y;
        float currentPosZ = transform.position.z;

        transform.position = new Vector3(_lanePoint[_lane], currentPosY, currentPosZ);
    }

    #region DoMovementSummary
    /// <summary>
    /// Move player
    /// </summary>
    #endregion
    public void DoMovement()
    {

        transform.Translate(Vector3.forward * Time.deltaTime * _speed, Space.World);

        transform.DOMoveX(_lanePoint[_lane], _changeLaneDuration);

        isOnGround = Physics.Raycast(transform.position, Vector3.down, 1f, _groundMask);

        LaneChanger();

    }

    #region LaneChangerSummary
    /// <summary>
    /// Change lane based on line points value
    /// </summary>
    #endregion
    private void LaneChanger()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (_lane == 0) return;
            _lane--;
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            if (_lane == _lanePoint.Length - 1) return;
            _lane++;
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            if (!isOnGround) return;

            rb.AddForce(transform.up * _jumpForce, ForceMode.Impulse);
            
        }
    }

}
